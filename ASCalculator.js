var acc = document.getElementsByClassName("accordion");
var i;
for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
        if (panel.style.maxHeight) {
            panel.style.maxHeight = null;
        } 
        else {
            panel.style.maxHeight = panel.scrollHeight + "px";
        }
    })
  } 

function WhatServices(evt, TypeOfServices) {
var i, tablinks, tabcontent;
tabcontent = document.getElementsByClassName("tabcontent");
for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
}
tablinks = document.getElementsByClassName("tablinks");
for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" current", "");
}
document.getElementById(TypeOfServices).style.display = "block";
evt.currentTarget.className += " current"; //add another className
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

var Sliders = document.getElementsByClassName("slider");
function SliderHandler() {
    var SliderId = this.id;
    var Output = document.getElementById(SliderId + "Val");
    Output.innerHTML = this.value;
}
for (var i = 0; i < Sliders.length; i++) {
Sliders[i].oninput = SliderHandler;
}

// reseting visible slider values
function sliderValRes() {
    var valArr = document.getElementsByClassName("slidVal");
    for (let i=0, len=valArr.length; i<len; i++) {
        valArr[i].innerHTML = "0";
    }
// and the statements
    var statArr = document.getElementsByClassName("statements");
    for (let j=0, len=statArr.length; j<len; j++) {
        statArr[j].innerHTML = "";
    }
}


// reseting the form
document.getElementById("resBut").addEventListener("click", function() {
    document.getElementById("Calculator").reset();
    sliderValRes();
});