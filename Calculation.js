function KPiR() {
    var State = document.getElementById("A1Statement");
    State.innerHTML = "";
    var A1 = 0;
    var Item = document.getElementById("A1");
    if (Item.value > 0) {
        if (Item.value <= 25)
            A1 = 350.00;
        else if (Item.value <= 50)
            A1 = 450.00;
        else if (Item.value <= 100)
            A1 = 650.00;
        else {
            A1 = 0;
            State.innerHTML = "  Price will be discussed individually.";
        } 
    }
    return A1;
}

function KH() {
    var State = document.getElementById("A2Statement");
    State.innerHTML = "";
    var A2 = 0;
    var Item = document.getElementById("A2");
    if (Item.value > 0) {
        if (Item.value <= 25)
            A2 = 750.00;
        else if (Item.value <= 50)
            A2 = 1000.00;
        else if (Item.value <= 100)
            A2 = 1500.00;
        else if (Item.value <= 150) 
            A2 = 2000.00;
        else {
            A2 = 0;
            State.innerHTML = "  Price will be discussed individually.";
        } 
    }
    return A2;
}

function RachPodJednZDostawa() {
    function RachPodJedn() {
        var Price = 0;
        if (document.getElementById("A31").checked == true)
            Price += 25.00;
        if (document.getElementById("A32").checked == true)
            Price += 25.00;
        if (document.getElementById("A33").checked == true)
            Price += 20.00;
        if (document.getElementById("A34").checked == true)
            Price += 30.00;
        if (document.getElementById("A35").checked == true)
            Price += 150.00;
        return Price;
    }
    var Index = document.getElementById("A4").selectedIndex;
    var Percent = 0;
    switch (Index){
        case 0: Percent = 0; break;
        case 1: Percent = 0.1; break;
        case 2: Percent = 0.25; break;
        case 3: Percent = 0.5; break;
    }
    var Price = RachPodJedn();
    return Price + Price * Percent;
}

function TotalA() {
    var APrice = KPiR() + KH() + RachPodJednZDostawa();
    return APrice;
}

function RozlWspolnika() {
    var Price = 0;
    if (document.getElementById("B1").checked == true)
        Price = 50.00;
    return Price;
}

    function OKP() {
        var B2 = 0;
        var Item = document.getElementById("B2");
        if (Item.value > 0) {
            if (Item.value <= 3)
                B2 = 240.00;
            else if (Item.value <= 20)
                B2 = Item.value * 80.00;
            else
                B2 = 20 * 80.00 + (Item.value - 20) * 100.00;
        }
        return B2;
    }

    function OP() {
        var B3 = 0;
        var Item = document.getElementById("B3");
        if (Item.value > 0) {
            if (Item.value <= 3)
                B3 = 180.00;
            else
                B3 = Item.value * 60.00;
        }
        return B3;
    }

    function KadrPlacJedn() {
        var Price = 0;
        if (document.getElementById("B41").checked == true)
            Price += 100.00;
        if (document.getElementById("B42").checked == true)
            Price += 50.00;
        if (document.getElementById("B43").checked == true)
            Price += 15.00;
        if (document.getElementById("B44").checked == true)
            Price += 20.00;
        if (document.getElementById("B45").checked == true)
            Price += 150.00;
        return Price;
    }

    function PorzTeczOs() {
        var Price = document.getElementById("B5").value * 250.00;
        return Price;
    }

    function PIT11() {
        var Price = document.getElementById("B6").value * 25.00;
        return Price;
    }

function TotalB() {
    var APrice = RozlWspolnika() + OKP() + OP() + KadrPlacJedn() + PorzTeczOs() + PIT11();
    return APrice;
}

function TotalC() {
    var CPrice = 0;
    if (document.getElementById("C1").checked == true)
        CPrice += 150.00;
    if (document.getElementById("C2").checked == true)
        CPrice += 150.00;
    if (document.getElementById("C3").checked == true)
        CPrice += 150.00;
    if (document.getElementById("C4").checked == true)
        CPrice += 400.00;
    if (document.getElementById("C6").checked == true)
        CPrice += 750.00;
    if (document.getElementById("C7").checked == true)
        CPrice += 200.00;
    if (document.getElementById("C8").checked == true)
        CPrice += 250.00;
    if (document.getElementById("C9").checked == true)
        CPrice += 500.00;
    if (document.getElementById("C10").checked == true)
        CPrice += 1000.00;
    if (document.getElementById("C11").checked == true)
        CPrice += 500.00;
    if (document.getElementById("C12").checked == true)
        CPrice += 50.00;
    if (document.getElementById("C13").checked == true)
        CPrice += 150.00;
    if (document.getElementById("C14").checked == true)
        CPrice += 50.00;
    return CPrice;
}

function TotalABC() {
    var ABCPrice = TotalA() + TotalB() + TotalC();
    document.getElementById("cost").value = ABCPrice;
    // alert(ABCPrice); has been replaced by real time visible input
}

var MyForm = document.getElementById("Calculator");
MyForm.addEventListener('change', TotalABC);